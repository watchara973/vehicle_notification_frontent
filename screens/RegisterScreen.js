import React from 'react';
import {  Alert, Button, TextInput, View, StyleSheet,Text 
  ,KeyboardAvoidingView } from 'react-native';
// const apiURL = ("http://192.168.31.72:8080/api/user")
import {apiURL} from '../components/UpdateToken'
async function  insertUser(params) {
    try {
      let response = await fetch(apiURL,{
          method: 'POST',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify(params)
      });
      let responseJson = await response.json();
      console.log(responseJson)
      return responseJson.result;
    } catch (error) {
      Alert.alert('Network Interruption!!!', "Can't add data");
      console.error(error)
    }
  }
export {insertUser};
export default class RegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: '',
      lname: '',
      typecar: '',
      gen: '',
      carID: '',
      username: '',
      password: '',  
    };
  }
  
  
  setData(){
    const { fname,lname,typecar,gen,carID,username,password} = this.state;
    if(username == ''){
      Alert.alert('Failed', "please input username");
      return null
    }
    if(password == ''){
      Alert.alert('Failed', "please input password");
      return null
    }
    if(fname == ''){
      Alert.alert('Failed', "please input firstname");
      return null
    }
    if(lname == ''){
      Alert.alert('Failed', "please input lastname");
      return null
    }
    
    const data = {fname:fname,
                  lname:lname,
                  typecar:typecar,
                  gen:gen,
                  carID:carID,
                  username:username,
                  password:password}
    console.log(insertUser(data));
    this.setState({fname: '',lname: '',typecar: '',gen: '',carID: '',username: '',password: ''})
    Alert.alert('Completed', "Registration Successful");
    this.props.navigation.navigate('Home')
  }
  static navigationOptions = {
    title: 'Register',
  }; 
  render() {
    return (
      <View style={styles.form}>
      <KeyboardAvoidingView behavior= 'position'>
       {/* <Image style={styles.img} source={require('../assets/logo1.jpg')} /> */}
      <View style={styles.textInput}>
      <Text>Username</Text>
        <TextInput
          value={this.state.username}
          onChangeText={(username) => this.setState({ username })}
          // placeholder={'Username'}
          style={styles.input}
          maxLength = {30}
        />
      </View>
      <View style={styles.textInput}>
      <Text>Password</Text>
        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          // placeholder={'Password'}
          secureTextEntry={true}
          style={styles.input}
          maxLength = {30}
        />
        </View>
        <View style={styles.textInput}>
        <Text>Firstname</Text>
        <TextInput
          value={this.state.fname}
          onChangeText={(fname) => this.setState({ fname })}
          // placeholder={'Firstname'}
          style={styles.input}
          maxLength = {30}
        />
        </View>
        <View style={styles.textInput}>
        <Text>Lastname</Text>
        <TextInput
          value={this.state.lname}
          onChangeText={(lname) => this.setState({ lname })}
          // placeholder={'Lastname'}
          style={styles.input}
          maxLength = {30}
        />
        </View>
        {/* <TextInput
          value={this.state.gen}
          onChangeText={(gen) => this.setState({ gen })}
          placeholder={'gen'}
          style={styles.input}
          maxLength = {20}
        />
        <TextInput
          value={this.state.typecar}
          onChangeText={(typecar) => this.setState({ typecar })}
          placeholder={'Brand'}
          style={styles.input}
          maxLength = {20}
        />
        <TextInput
          value={this.state.carID}
          onChangeText={(carID) => this.setState({ carID })}
          placeholder={'carID'}
          style={styles.input}
          maxLength = {30}
        /> */}
        
        <View style={styles.Button}>
        <Button
          title={'Register'}
          onPress={() => this.setData()}
        /></View>
        </KeyboardAvoidingView>
        
        </View>
        
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  form:{
    alignSelf: 'stretch',
    alignItems: 'center'
  },
  textInput: {
    flexDirection: 'row',
    marginTop: 20,
  },
  input: {
    width: 170,
    paddingLeft: 20,
    borderBottomColor: '#000',
    borderBottomWidth: 4,
    alignSelf: 'stretch'
  },
  Button: {
    paddingTop: 20,
    marginTop: 20
  },
});
