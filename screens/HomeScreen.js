import React from 'react';
import { StyleSheet, Text, View,Alert,TouchableOpacity,Image,AsyncStorage } from 'react-native';

export default class HomeScreen extends React.Component {
  clearAsyncStorage = async() => {
    AsyncStorage.clear();
}
componentWillMount() {
  this.clearAsyncStorage();
}
    static navigationOptions = {
        title: 'Vehicle Notification',
      };
      render() {
        return (  
          <View style={styles.container}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Remote')}>
          <Image style={styles.img} source={require('../assets/logo1.jpg')} />
          </TouchableOpacity>
          <View style={styles.buttonView}>
          <TouchableOpacity
        onPress={() => this.props.navigation.navigate('Login')}
        style={styles.button}>
          <Text style={styles.text}>Login</Text>
        </TouchableOpacity>
          </View>
          <View style={styles.buttonView}>
          <TouchableOpacity
        onPress={() => this.props.navigation.navigate('Register')}
        style={styles.button}
        >
          <Text style={styles.text}>Register</Text>
        </TouchableOpacity>
          </View>
        </View>
        );
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems:'center',
    
  },
  img: {
    height: 280,
    width: 280,
  },
  buttonView: {
    marginTop : 10,
  },
  button: {
    margin:10,
    height:50,
    width:200,
    backgroundColor: 'dodgerblue',
    justifyContent:'center',
    borderColor: 'dodgerblue',
    borderWidth:2
  },
  text:{
    color:"white",
    textAlign:'center',
    fontSize:18
  },

});