import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet,Text 
  ,AsyncStorage,Image,KeyboardAvoidingView,ScrollView,RefreshControl} from 'react-native';
  import { Permissions, Notifications } from 'expo';
  import {apiURL1} from '../components/UpdateToken'
export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      fname: '',
      lname: '',
      typecar: '',
      gen: '',
      carID: '', 
      data: [0],
      id: 0,
      token: "",
    };
  }
  
  componentWillMount() {
    try {
      this.fetchData();
      this.registerForPushNotificationsAsync()
      
    } catch (error) {
      Alert.alert('Network Interruption!!!', "No data");
      console.log(error.message);
    }
    
  }

  fetchData = async () => {
    const response = await fetch(apiURL1);
    const json = await response.json();
    this.setState({ data: json });
  }
  deleteUserId = async () => {
    try {
      await AsyncStorage.removeItem('myProfile');
    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }
  }
   registerForPushNotificationsAsync = async() =>{
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  // console.log(existingStatus);
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }

  // Get the token that uniquely identifies this device
//   alert('asking');
  let token = await Notifications.getExpoPushTokenAsync();
//   alert('token : ' + token);
  // POST the token to your backend server from where you can retrieve it to send push notifications.
  
  this.setState({token: token})
  console.log(this.state.token)
}
  onLogin() {
    const { username, password,data,token} = this.state;
    try {
      for (let i = 0; i < data.length; i++){ 
      let d = data[i];
      if (username == d['username'] && password == d['password']){
        // console.log(d)
        let myProfile ={
          id: d['id'],
          fname: d['fname'],
          lname: d['lname'],
          carID: d['carID'],
          username: d['username'],
          password: d['password'],
          typecar: d['typecar'],
          gen: d['gen'],
          token: ""
        }
        myProfile.token = token
        AsyncStorage.setItem('myProfile',JSON.stringify(myProfile)); // save 
        // console.log(myProfile)
        this.props.navigation.navigate('Main') // route to main
        this.setState({username: '',password: ''}) //clear text
        break;
        } 
      if(i == data.length -1 ){
        Alert.alert('Wrong Username or Password!!!', "Please password again");
        }
      }  
    } catch (error) {
      Alert.alert('Network Interruption!!!', "Please connect the network");
    }
    
    
  }
  static navigationOptions = {
    title: 'Login',
  };
  render() {
    return ( 
      <View style={styles.form}>
      <KeyboardAvoidingView behavior= 'position'>
       <Image style={styles.img} source={require('../assets/logo1.jpg')} />
       <View style={styles.textInput}>
         <Text>Username</Text>
        <TextInput
          value={this.state.username}
          onChangeText={(username) => this.setState({ username })}
          // placeholder={'Username'}
          style={styles.input}
          maxLength = {30}
        />
        </View>
        <View style={styles.textInput}>
        <Text>Password</Text>
        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          // placeholder={'Password'}
          secureTextEntry={true}
          style={styles.input}
        />
        </View>
        <View style={styles.Button}>
        <Button
          title={'Login'}
          type='clear'
          onPress={() => this.onLogin()}
        /></View>
        </KeyboardAvoidingView>
        
        </View>
        
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  form:{
    alignSelf: 'stretch',
    alignItems: 'center'
  },
  logo: {
    flex: 1,
    alignItems: 'center',
  },
  textInput: {
    flexDirection: 'row',
    marginTop: 10,
  },
  login:{
    fontSize: 24,
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 10,
    marginTop: 20,
    
  },
  input: {
    width: 170,
    marginLeft: 30,
    paddingLeft: 20,
    borderColor: '#000',
    borderBottomWidth: 4,
  },
  img: {
    height: 280,
    width: 280,
  },
  Button: {
    paddingTop: 20,
    marginTop: 20
  },
});
