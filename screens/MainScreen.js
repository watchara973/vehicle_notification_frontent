import React from 'react';
import { StyleSheet, Text, View,FlatList,AsyncStorage,Alert,Image,TouchableOpacity,ScrollView, RefreshControl} from 'react-native';
import LocalNotification from "../components/LocalNotification";
import ManageCar from "../components/ManageCar";
import UpdateToken from "../components/UpdateToken"
export default class MainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }
  clearAsyncStorage = async() => {
    AsyncStorage.clear();
}
_onRefresh = () => {
  this.setState({refreshing: true});
  //fetchData().then(() => {
    this.setState({refreshing: false});
  //});
}
static navigationOptions = {
  title: 'Vehicle Notification',
};
  render() {
    return (
      <View style={styles.container}>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          >
          </RefreshControl>
        }>
      {/* <UpdateToken/> */}
      <ManageCar/>
      <LocalNotification/>
      <View style={styles.ButtonView}>
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('Update')}
        style={styles.button}>
          <Text style={styles.text}>Update</Text>
        </TouchableOpacity>
        <TouchableOpacity
        onPress={() => this.props.navigation.navigate('Home')}
        style={styles.button}>
          <Text style={styles.text}>Logout</Text>
        </TouchableOpacity>
        </View>
        </ScrollView>
      </View>
    );
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ButtonView: {
    flexDirection: 'row',
    backgroundColor: '#ecf0f1',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    margin:10,
    height:50,
    width:170,
    backgroundColor: 'dodgerblue',
    justifyContent:'center',
    borderColor: 'dodgerblue',
    borderWidth:2
  },
  text:{
    color:"white",
    textAlign:'center',
    fontSize:18
  },
});