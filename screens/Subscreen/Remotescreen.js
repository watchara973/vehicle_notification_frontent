import React from 'react';
import { StyleSheet, Text, View,FlatList,AsyncStorage,Alert,Image,TouchableOpacity,ScrollView, RefreshControl} from 'react-native';
import RemoteNotification from "../../components/RemoteNotification";
export default class Remotescreen extends React.Component {
static navigationOptions = {
  title: 'Remote Notification',
};
  render() {
    return (
      <View style={styles.container}>
      <RemoteNotification/>
      </View>
    );
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    alignItems: 'center',
    justifyContent: 'center',
  },
});