import React from 'react';
import { createStackNavigator , createAppContainer} from 'react-navigation';

import MainScreen from '../screens/MainScreen';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import HomeScreen from '../screens/HomeScreen';
import UpdateUser from "../components/UpdateUser";
import Remotescreen from "../screens/Subscreen/Remotescreen";

const Navigator = createStackNavigator({
      Home: { 
        screen: HomeScreen 
      },
      Login: {
        screen: LoginScreen
      },
      Main: {
        screen: MainScreen
        },
      Register: {
        screen: RegisterScreen
      },
      Update: {
        screen: UpdateUser
      },
      Remote: {
        screen: Remotescreen
      }
    }
);

export default createAppContainer(Navigator);