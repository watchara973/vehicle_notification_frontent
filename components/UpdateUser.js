import React from 'react';
import {  Alert, Button, TextInput, View, StyleSheet,Text 
  ,KeyboardAvoidingView,AsyncStorage } from 'react-native';
import {apiURL} from './UpdateToken'

async function UpdateUserdata(params) {
    try {
      let response = await fetch(apiURL,{
          method: 'PUT',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify(params)
      });
      let responseJson = await response.json();
      console.log(responseJson)
      return responseJson.result;
    } catch (error) {
      Alert.alert('Network Interruption!!!', "Can't Upadate data");
      console.error(error)
    }
  }
export {UpdateUserdata};
export default class UpdateUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:0,
      fname: '',
      lname: '',
      typecar: '',
      gen: '',
      carID: '',
      username: '',
      password: '',
      token: '' 
    };
  }
  componentWillMount() {
    this.showData();
  }
  setData(){
    const { id,fname,lname,typecar,gen,carID,username,password,token} = this.state;
    const data = {id:id,
                  fname:fname,
                  lname:lname,
                  typecar:typecar,
                  gen:gen,
                  carID:carID,
                  username:username,
                  password:password,
                  token:token}
    console.log(UpdateUserdata(data));
    try {
        AsyncStorage.setItem('myProfile',JSON.stringify(data)); // save data
        }
     catch (error) {
      Alert.alert('Network Interruption!!!', "Please connect the network");
    }
    Alert.alert('Completed', "Update Successful");
    this.props.navigation.navigate('Login')
    this.props.navigation.navigate('Main')
  }
  showData = async()=> {
    const myProfile = await AsyncStorage.getItem('myProfile');
    const data = JSON.parse(myProfile)
    // this.setState({d: data});
    // console.log(data);
    this.setState({ id: data.id,
                    fname: data.fname,
                    lname: data.lname,
                    carID: data.carID,
                    typecar:data.typecar,
                    gen:data.gen,
                    username:data.username,
                    password:data.password,
                    token:data.token})
    //Alert.alert(this.state.message)
    console.log(data);
    
    
  } 
  static navigationOptions = {
    title: 'Update',
  };
  render() {
    return (
      <View style={styles.form}>
      <KeyboardAvoidingView behavior= 'position'>
       {/* <Image style={styles.img} source={require('../assets/logo1.jpg')} /> */}
       <View style={styles.textInput}>
         <Text>Generation</Text>
        <TextInput
          value={this.state.gen}
          onChangeText={(gen) => this.setState({ gen })}
          placeholder={'gen'}
          style={styles.input}
          maxLength = {20}
        />
        </View>
        <View style={styles.textInput}>
        <Text>Brand</Text>
        <TextInput
          value={this.state.typecar}
          onChangeText={(typecar) => this.setState({ typecar })}
          placeholder={'Brand'}
          style={styles.input}
          maxLength = {20}
        />
        </View>
        <View style={styles.textInput}>
        <Text>carID</Text>
        <TextInput
          value={this.state.carID}
          onChangeText={(carID) => this.setState({ carID })}
          placeholder={'carID'}
          style={styles.input}
          maxLength = {30}
        />
        </View>
        <View style={styles.Button}>
        <Button
          title={'Update'}
          onPress={() => this.setData()}
        /></View>
        </KeyboardAvoidingView>
        
        </View>
        
    );
  }
}

const styles = StyleSheet.create({
  form:{
    flex: 1,
    alignSelf: 'stretch',
  },
  input: {
    marginLeft: 20,
    borderBottomColor: '#000',
    borderBottomWidth: 4,
    alignSelf: 'stretch',
    width: 100,
    paddingLeft: 20,
  },
  textInput: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 140,
  },
  Button: {
    marginLeft: 100,
    paddingTop: 20,
    marginTop: 20,
    width: 220
  },
});
