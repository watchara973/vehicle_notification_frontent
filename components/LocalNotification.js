import React, { Component } from 'react';
import { View, StyleSheet, Button, Alert, Platform,Vibration,Text,TouchableOpacity,AsyncStorage } from 'react-native';
import { Constants, Notifications, Permissions } from 'expo';
import {apiURL} from '../components/UpdateToken'
async function getiOSNotificationPermission() {
  const { status } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  if (status !== 'granted') {
    await Permissions.askAsync(Permissions.NOTIFICATIONS);
  }
}

export default class LocalNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id:0,
      fname: '',
      lname: '',
      typecar: '',
      gen: '',
      carID: '',
      username: '',
      password: '',
      token: '',  
      toggle:false
    };
  }
  setData(){
    const { id,fname,lname,typecar,gen,carID,username,password,token} = this.state;
    const data = {id:id,fname:fname,lname:lname,typecar:typecar,gen:gen,carID:carID,
                  username:username,password:password,token:token}
    console.log(data.token)
    this.UpdateToken(data)
  }
  UpdateToken = async(params) =>{
  try {
    let response = await fetch(apiURL,{
        method: 'PUT',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
    });
    let responseJson = await response.json();
    console.log(responseJson)
    return responseJson.result;
  } catch (error) {
    Alert.alert('Network Interruption!!!', "Can't Upadate Token");
    console.error(error)
  }
}
    showData = async()=> {
    const myProfile = await AsyncStorage.getItem('myProfile');
    const data = JSON.parse(myProfile)
    // this.setState({d: data});
    // console.log(data);
    this.setState({ id: data.id,
                    fname: data.fname,
                    lname: data.lname,
                    carID: data.carID,
                    typecar:data.typecar,
                    gen:data.gen,
                    username:data.username,
                    password:data.password,
                    token:data.token})

    //Alert.alert(this.state.message)
    //console.log(data);
    } 
    
        
        
    
    _onPress = () => {
      if(this.state.typecar == ""){
        Alert.alert("Warning!! You don\'t have any car.", "Please update your car\'s data.")
        return;
      }
      const newState = !this.state.toggle;
      this.setState({toggle:newState});
      this.setData();
    }
    _handleButtonPress = () =>{
      if(this.state.typecar == ""){
        Alert.alert("Warning!! You don\'t have any car.", "Please update your car\'s data.")
        return;
      }
      const {toggle} = this.state;
      const text = toggle?"ON":"OFF";
      if (text == "OFF"){
        Alert.alert("Warning!!", "You haven\'t turned on notification.")
        return ;
      }
      const localnotification = {
        title: 'Notify Example',
        body: 'your car ('+this.state.carID+') is Turned '+text,
        android: {
          sound: true,
          vibrate: true,
        },
        ios: {
          sound: true,
        },
        // data: {
        //   title: 'Hello',
        //   body: 'world'
        // }
      };
    let sendAfterSeconds = Date.now();
    sendAfterSeconds += 3000;
    const PATTERN = [1000, 2000, 1000];
    const schedulingOptions = { time: sendAfterSeconds ,
                                repeat: "minute"};
    Alert.alert("Successful", "System\'s sending a notification Message.")
    Notifications.scheduleLocalNotificationAsync(
      localnotification,
      schedulingOptions
    );
    Vibration.vibrate(PATTERN)
    
  };
  listenForNotifications = () => {
    Notifications.addListener(notification => {
      console.log(notification)
      var data = notification.data
      if (notification.origin === 'received' && Platform.OS === 'ios') {
        Alert.alert(data.title, data.body);
      }
    });
  };
  componentWillMount() {
    getiOSNotificationPermission();
    this.listenForNotifications();
    this.showData();
  }
  
  render() {
    const {toggle} = this.state;
    const textValue = toggle?"ON":"OFF";
    const buttonBg = toggle?"dodgerblue":"red";
    const textColor = toggle?"white":"black";
    return (
      <View style={styles.container}>
        <Button
          title="Send a notification"
          onPress={this._handleButtonPress}/>
        <TouchableOpacity
        onPress={this._onPress}
        style={{margin:10,height:50,width:100,backgroundColor:buttonBg,justifyContent:'center'}}>
          <Text style={{color:textColor,textAlign:'center',fontSize:16}}>{textValue}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    // backgroundColor: '#ecf0f1',
  }
});