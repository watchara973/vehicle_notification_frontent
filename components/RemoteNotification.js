import React from 'react';
import { StyleSheet, Text, View,FlatList,AsyncStorage,Alert,Image,TouchableOpacity,ScrollView, RefreshControl} from 'react-native';
import { Permissions, Notifications } from 'expo';

const PUSH_ENDPOINT = 'https://192.168.31.72:8080/api/user';

export async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  console.log(existingStatus);
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }

  // Get the token that uniquely identifies this device
  alert('asking');
  let token = await Notifications.getExpoPushTokenAsync();
  alert('token : ' + token);
  // POST the token to your backend server from where you can retrieve it to send push notifications.
  return fetch(PUSH_ENDPOINT, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      token: {
        value: token,
      },
    }),
  });
}

export default class RemoteNotification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }
static navigationOptions = {
  title: 'Remote Notification',
};
  render() {
    return (
      <TouchableOpacity onPress = {() =>registerForPushNotificationsAsync() }>
          <Text>Send Notification</Text>
      </TouchableOpacity>
    );
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    margin:10,
    height:50,
    width:200,
    backgroundColor: 'dodgerblue',
    justifyContent:'center',
    borderColor: 'dodgerblue',
    borderWidth:2
  },
  text:{
    color:"white",
    textAlign:'center',
    fontSize:18
  },
});