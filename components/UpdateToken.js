import React from 'react';
import {  Alert, Button, TextInput, View, StyleSheet,Text 
  ,KeyboardAvoidingView,AsyncStorage } from 'react-native';
//  export const apiURL = 'http://192.168.43.75:8080/api/user'
//  export const apiURL1 = 'http://192.168.43.75:8080/api/users'
 export const apiURL = 'http://ef7d3ada.ngrok.io/api/user'
 export const apiURL1 = 'http://ef7d3ada.ngrok.io/api/users'
//  const apiURL = ("http://192.168.31.72:8080/api/user");
// export const apiURL = 'http://192.168.31.72:8080/api/user'
// export const apiURL1 = 'http://192.168.31.72:8080/api/users'
export default class UpdateUser extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        id:0,
        fname: '',
        lname: '',
        typecar: '',
        gen: '',
        carID: '',
        username: '',
        password: '',
        token: '' 
      };
    }
    componentWillMount() {
      this.showData();
      this.setData();
    }
    setData(){
      const { id,fname,lname,typecar,gen,carID,username,password,token} = this.state;
      const data = {id:id,
                    fname:fname,
                    lname:lname,
                    typecar:typecar,
                    gen:gen,
                    carID:carID,
                    username:username,
                    password:password,
                    token:token}
      console.log(UpdateToken(data));
    }
    showData = async()=> {
      const myProfile = await AsyncStorage.getItem('myProfile');
      const data = JSON.parse(myProfile)
      // this.setState({d: data});
      // console.log(data);
      this.setState({ id: data.id,
                      fname: data.fname,
                      lname: data.lname,
                      carID: data.carID,
                      typecar:data.typecar,
                      gen:data.gen,
                      username:data.username,
                      password:data.password,
                      token:data.token})
      //Alert.alert(this.state.message)
      console.log(data);
    } 
      UpdateToken = async(params) =>{
    try {
      let response = await fetch(apiURL,{
          method: 'PUT',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify(params)
      });
      let responseJson = await response.json();
      console.log(responseJson)
      return responseJson.result;
    } catch (error) {
      Alert.alert('Network Interruption!!!', "Can't Upadate data");
      console.error(error)
    }
  }

    static navigationOptions = {
      title: 'Update',
    };
    render() {
      return (null);
    }
  }