import React from 'react';
import { StyleSheet, Text, View,FlatList,AsyncStorage,Alert,Image} from 'react-native';
export default class ManageCar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          d:{} , 
          //item:{},
          // id:0,
          // fname: '',
          // lname: '',
          // typecar: '',
          // gen: '',
          // carID: '',
          // username: '',
          // password: ''
        };
      }
      componentWillMount() {
        this.showData(); 
      }
  //     componentDidUpdate(){
  //       this.showData()
  //     }
  // refreshData(changeItem){
  //     this.setState({item:changeItem})
  //     }
  showData = async()=> {
    const myProfile = await AsyncStorage.getItem('myProfile');
    const data = JSON.parse(myProfile)
    // this.setState({d: data});
    //console.log(data);
    this.setState({d: { id: data.id,
                    fname: data.fname,
                    lname: data.lname,
                    carID: data.carID,
                    typecar:data.typecar,
                    gen:data.gen,
                    username:data.username,
                    password:data.password,
                    token: data.token}})
    // let selectItem = this.state.d.id ? this.state.d: this.props.d
    // this.refreshData(selectItem)
    //Alert.alert(this.state.message)
    //console.log(myProfile);
    // const { id,fname,lname,typecar,gen,carID,username,password } = this.state;
    // const d = { id:id,fname:fname,lname:lname,typecar:typecar,
    //             gen:gen,carID:carID,username:username,password:password }
  } 
  render() {
    const { d } = this.state;
    return (
      <View style={styles.container}>
      <Text style={styles.text}>User            : {d.fname} {d.lname} </Text> 
      <Text style={styles.text}>Typecar      : {d.typecar}</Text>
      <Text style={styles.text}>Generation : {d.gen}</Text>
      <Text style={styles.text}>CarID           : {d.carID}</Text>
      {/* <Text>Token     : {d.token}</Text> */}
      </View>
    );
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    paddingLeft: 70
  },
  text:{
    color:"black",
    textAlign: 'left',
    fontSize:24
  },
});